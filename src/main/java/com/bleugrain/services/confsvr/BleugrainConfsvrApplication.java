package com.bleugrain.services.confsvr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class BleugrainConfsvrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BleugrainConfsvrApplication.class, args);
	}

}
